import HeaderStyle from "./style/HeaderStyle";
import { BigButton, NormalButton } from "./style/Button";
import ContainerStyle from "./style/ContainerStyle";

function App() {
  return (
    <div>
      <ContainerStyle>
        <HeaderStyle />
          <div style={{width: "263px", height:"60px"}}>
            <BigButton style={{marginLeft: "10px"}}>DEL</BigButton>
            <NormalButton style={{marginLeft: "1px"}}>-</NormalButton>
            <NormalButton math style={{marginLeft: "1px"}}>/</NormalButton>
          </div>
          <div style={{width: "263px", height:"60px", marginTop:"1px"}}>
            <NormalButton style={{marginLeft: "10px"}}>7</NormalButton>
            <NormalButton style={{marginLeft: "1px"}}>8</NormalButton>
            <NormalButton style={{marginLeft: "1px"}}>9</NormalButton>
            <NormalButton math style={{marginLeft: "1px"}}>*</NormalButton>
          </div>
          <div style={{width: "263px", height:"60px", marginTop:"1px"}}>
            <NormalButton style={{marginLeft: "10px"}}>4</NormalButton>
            <NormalButton style={{marginLeft: "1px"}}>5</NormalButton>
            <NormalButton style={{marginLeft: "1px"}}>6</NormalButton>
            <NormalButton math style={{marginLeft: "1px"}}>-</NormalButton>
          </div>
          <div style={{width: "263px", height:"60px", marginTop:"1px"}}>
            <NormalButton style={{marginLeft: "10px"}}>1</NormalButton>
            <NormalButton style={{marginLeft: "1px"}}>2</NormalButton>
            <NormalButton style={{marginLeft: "1px"}}>3</NormalButton>
            <NormalButton math style={{marginLeft: "1px"}}>+</NormalButton>
          </div>
          <div style={{width: "263px", height:"60px", marginTop:"1px"}}>
            <NormalButton style={{marginLeft: "10px"}}>0</NormalButton>
            <NormalButton style={{marginLeft: "1px"}}>.</NormalButton>
            <BigButton gradient style={{marginLeft:"1px"}}>=</BigButton>
          </div>
      </ContainerStyle>
    </div>
  );
}

export default App;
