import styled from "styled-components";

const BigButton = styled.button`
    background-color: ${props => props.gradient ? "#4F9492" : "#CED2D8"};
    color: ${props => props.gradient ? "#ffffff" : "#6F7070"};  
    height: 60px;
    width: 120px;
    font-weight: 600;
    font-size: 18px;
    border: none;
    
`
const NormalButton = styled.button`
    background-color: ${props => props.math ? "#B9D7D5" : "#E5E3EA"};
    color: ${props => props.math ? "#727978" : "#569594" };
    height: 60px;
    width: 60px;
    font-size: 18px;
    font-weight: 700;
    padding: 19px;
    border: none;
    
`

export { BigButton, NormalButton }